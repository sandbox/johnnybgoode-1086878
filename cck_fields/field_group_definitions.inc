$g = array (
  'label' => 'OCR',
  'group_type' => 'standard',
  'settings' => array (
    'form' => array (
      'style' => 'fieldset_collapsible',
      'description' => '',
    ),
    'display' => array (
      'description' => '',
      'teaser' => array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'token' => array (
        'format' => 'fieldset',
        'exclude' => 0,
      ),
      'label' => 'above',
    ),
  ),
  'weight' => '40',
  'group_name' => 'group_ocr',
);
